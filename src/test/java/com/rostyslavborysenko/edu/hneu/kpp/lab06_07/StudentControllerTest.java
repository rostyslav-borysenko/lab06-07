package com.rostyslavborysenko.edu.hneu.kpp.lab06_07;

import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.controller.StudentController;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.StudentDto;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.entity.Student;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.service.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.ui.Model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StudentControllerTest {

    @Mock
    private StudentService studentService;

    @Mock
    private Model model;

    @InjectMocks
    private StudentController studentController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private static final ModelMapper mapper = new ModelMapper();

    @Test
    void testShowHomePage() {
        String viewName = studentController.showHomePage();
        assertEquals("index", viewName);
    }

    @Test
    void testShowInsertForm() {
        String viewName = studentController.showInsertForm(model);
        assertEquals("insert", viewName);
        verify(model, times(1)).addAttribute(eq("student"), any(StudentDto.class));
    }

    @Test
    void testCreateStudent() {
        StudentDto studentDto = new StudentDto();
        studentDto.setFirstName("John");

        Student student = mapper.map(studentDto, Student.class);

        String viewName = studentController.createStudent(studentDto, model);

        assertEquals("redirect:/students", viewName);
        verify(studentService, times(1)).save(student);
    }

    @Test
    void testShowEditForm() {
        Integer id = 1;
        Student student = new Student();
        student.setId(id);
        student.setFirstName("John");

        when(studentService.findById(id)).thenReturn(student);

        String viewName = studentController.showEditForm(id.toString(), model);

        assertEquals("insert", viewName);
        verify(model, times(1)).addAttribute(eq("student"), any(StudentDto.class));
    }

    @Test
    void testUpdateStudent() {
        Integer id = 1;
        StudentDto studentDto = new StudentDto();
        studentDto.setId(id);
        studentDto.setFirstName("John");

        Student student = mapper.map(studentDto, Student.class);

        String viewName = studentController.updateStudent(id, studentDto, model);

        assertEquals("redirect:/students", viewName);
        verify(studentService, times(1)).save(student);
    }

    @Test
    void testDeleteStudent() {
        Integer id = 1;

        String viewName = studentController.deleteStudent(id);

        assertEquals("redirect:/students", viewName);
        verify(studentService, times(1)).deleteById(id);
    }
}