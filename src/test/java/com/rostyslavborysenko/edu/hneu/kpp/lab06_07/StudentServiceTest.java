package com.rostyslavborysenko.edu.hneu.kpp.lab06_07;

import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.entity.Student;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.repository.StudentRepository;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.service.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentService studentService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testSaveStudent() {
        Student student = new Student();
        student.setId(1);
        student.setFirstName("John");

        when(studentRepository.save(student)).thenReturn(student);

        studentService.save(student);

        verify(studentRepository, times(1)).save(student);
    }

    @Test
    void testFindStudentById() {
        Student student = new Student();
        student.setId(1);
        student.setFirstName("John");

        when(studentRepository.findById(1)).thenReturn(Optional.of(student));

        Student foundStudent = studentService.findById(1);

        assertEquals(student, foundStudent);
    }

    @Test
    void testDeleteStudentById() {
        studentService.deleteById(1);
        verify(studentRepository, times(1)).deleteById(1);
    }

    @Test
    void testFindAllStudents() {
        List<Student> students = new ArrayList<>();
        students.add(new Student());
        students.add(new Student());

        when(studentRepository.findAll()).thenReturn(students);

        List<Student> foundStudents = studentService.findAll();

        assertEquals(students.size(), foundStudents.size());
    }
}