package com.rostyslavborysenko.edu.hneu.kpp.lab06_07.repository;

import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
}
