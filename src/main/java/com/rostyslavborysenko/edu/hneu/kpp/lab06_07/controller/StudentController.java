package com.rostyslavborysenko.edu.hneu.kpp.lab06_07.controller;

import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.StudentDto;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.entity.Student;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.service.StudentService;
import jakarta.servlet.RequestDispatcher;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    private StudentService studentService;

    @GetMapping("")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/students")
    public String showAllStudents(Model model) {
        model.addAttribute("students", studentService.findAll());
        return "showAll";
    }

    @GetMapping("/students/insert")
    public String showInsertForm(Model model) {
        model.addAttribute("student", new StudentDto());
        return "insert";
    }

    @PostMapping("/students")
    public String createStudent(@ModelAttribute("student") StudentDto studentDto, Model model) {
        List<String> errors = findErrors(studentDto);
        if(!errors.isEmpty()) {
            model.addAttribute("errors", errors);
            return "insert";
        }

        Student student = mapper.map(studentDto, Student.class);
        studentService.save(student);
        return "redirect:/students";
    }

    @GetMapping("/students/edit/{id}")
    public String showEditForm(@PathVariable("id") String idString, Model model) {
        Integer id = Integer.parseInt(idString);
        Student student = studentService.findById(id);
        model.addAttribute("student", mapper.map(student, StudentDto.class));
        return "insert";
    }

    @PostMapping("/students/{id}")
    public String updateStudent(@PathVariable("id") Integer id, @ModelAttribute("student") StudentDto studentDto, Model model) {
        List<String> errors = findErrors(studentDto);
        if(!errors.isEmpty()) {
            model.addAttribute("errors", errors);
            return "insert";
        }

        Student student = mapper.map(studentDto, Student.class);
        student.setId(id);
        studentService.save(student);
        return "redirect:/students";
    }

    @GetMapping("/students/delete/{id}")
    public String deleteStudent(@PathVariable("id") Integer id) {
        studentService.deleteById(id);
        return "redirect:/students";
    }

    private List<String> findErrors(StudentDto studentDto) {
        List<String> errors = new ArrayList<>();

        if(studentDto.getFirstName().length() > 20) {
            errors.add("First name can not be longer than 20 letters");
        }

        if(studentDto.getLastName().length() > 30) {
            errors.add("Last name can not be longer than 30 letters");
        }

        if(studentDto.getMiddleName().length() > 30) {
            errors.add("Middle name can not be longer than 30 letters");
        }

        if(studentDto.getPhoneNumber().length() > 13) {
            errors.add("Phone number can not be longer than 13 characters");
        }

        if(studentDto.getEmail().length() > 50) {
            errors.add("Email can not be longer than 50 characters");
        }

        if(studentDto.getSpeciality().length() > 50) {
            errors.add("Speciality can not be longer than 50 characters");
        }

        if(studentDto.getGroupNumber().length() > 50) {
            errors.add("Group number can not be longer than 50 characters");
        }

        Integer course = studentDto.getCourse();
        if(course < 1 || course > 6) {
            errors.add("Course must be between 1 and 6.");
        }

        if(studentDto.getDetails().length() > 255) {
            errors.add("Details can not be longer than 255 characters");
        }

        return errors;
    }
}
