package com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "middle_name", nullable = false)
    private String middleName;

    @Column(name = "sex")
    private String sex;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "speciality", nullable = false)
    private String speciality;

    @Column(name = "group_number", nullable = false)
    private String groupNumber;

    @Column(name = "course", nullable = false)
    private Integer course;

    @Column(name = "details")
    private String details;

    @Override
    public String toString() {
        return ("Student [id=%d, name=%s %s %s, sex=%s, phoneNumber=%s, " +
                            "email=%s, speciality=%s, groupName=%s, course=%d, details=%s]").formatted(
                                getId(), getFirstName(), getLastName(), getMiddleName(), getSex(),
                                getPhoneNumber(), getEmail(), getSpeciality(), getGroupNumber(),
                                getCourse(), getDetails());
    }
}
