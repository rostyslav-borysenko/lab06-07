package com.rostyslavborysenko.edu.hneu.kpp.lab06_07.service;

import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model.entity.Student;
import com.rostyslavborysenko.edu.hneu.kpp.lab06_07.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public void save(Student student) {
        studentRepository.save(student);
    }

    public Student findById(Integer id) {
        return studentRepository.findById(id).orElse(null);
    }

    public void deleteById(Integer id) {
        studentRepository.deleteById(id);
    }

    public List<Student> findAll() {
        return studentRepository.findAll();
    }
}
