package com.rostyslavborysenko.edu.hneu.kpp.lab06_07.model;

import lombok.Data;

@Data
public class StudentDto {
    private Integer id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String sex;
    private String phoneNumber;
    private String email;
    private String speciality;
    private String groupNumber;
    private Integer course;
    private String details;
}
